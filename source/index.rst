.. network-automation-by-example documentation master file, created by
   sphinx-quickstart on Tue Apr  2 16:01:34 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome
=======

This is my Network Automation By Example documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   documentation.md



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
