# Documentation as code

Let's start with documentation as code.

## Tools

I used the following tools.

- **[Python](https://www.python.org)** which came with macOS installed on my laptop
- **[git](https://git-scm.com/)** which also came with macOS
- **[GitLab](https://www.gitlab.com/)** to host my documentation sources, e.g., Sphinx binaries and raw documentation files in Markdown and reStructuredText formats, additional files used by Read the Docs platform to automatically render publicly consumable documentation
- **[VSCodium](https://vscodium.com)** where I write most of my code
- **[Zettlr](https://www.zettlr.com/)** which I use to create and update Markdown-based raw documentation files
- **[Sphinx](https://www.sphinx-doc.org/)** project to initially create a local documentation project and set up a git repository framework for Read the Docs engine
- **[Mermaid](https://mermaid.js.org/)** and **[sphinxcontrib-mermaid](https://pypi.org/project/sphinxcontrib-mermaid/)** to render diagrams from code embedded in Markdown documents
- **[Read the Docs](https://readthedocs.org)** platform to automatically render and publicize documentation

## Workflow

The diagram below depicts my standard documentation workflow using the tools enumerated earlier.

```{mermaid}
    flowchart TD
        subgraph laptop
            Python
            git
            VSCodium
            Zettlr
            Sphinx
            Mermaid
        end
        subgraph cloud
            GitLab
            ReadtheDocs
            publicDocumentation
        end   
        user -- create Markdown docs --> Zettlr
        user -- create Marmaid code diagrams --> VSCodium
        Zettlr <-- joint Markdown docs repo --> VSCodium
        VSCodium -- version control --> git
        VSCodium -- feed code for local docs build --> Sphinx
        Mermaid -- diagram rendering capability --> Sphinx
        git <-- remote local code repositories sync --> GitLab
        Python -- runtime environment --> Sphinx
        Sphinx -- docs creation framework --> VSCodium
        GitLab -- code, static and config --> ReadtheDocs
        ReadtheDocs -- automatic rendering --> publicDocumentation
```

## GitLab project

I created a separate project on GitLab to host files related to my documentation as code framework. These consist of file structure and source documentation and static files coming from the initial Sphinx installation. It also includes `make.bat` and `Makefile` files for local documentation build process, not needed for Read the Docs engine. Two additional files which do not come from Sphinx installation are required for Read the Docs – ` .readthedocs.yaml` and `requirements.txt`. Their role will be described later.

Project creation process within GitLab currently involves a wizard. I chose _Create blank project_ option, provided a suitable name, changed the default option of _Private_ in _Visibility Level_ section to _Public_ which is required by Read the Docs engine, and finally disabled _Initialize repository with a README_ option in _Project Configuration_ section. Pressing _Create project_ button creates the project after few seconds. The process is illustrated below.

![Create new GitLab project](_static/gitlab-create-a-new-project.png)

Once the project was created, I needed to make sure that GitLab already has the public key corresponding to the private key, which I use to secure ssh-based connectivity between GitLab and my laptop. Currently, this is in _Profile_ → _Preferences_ → _SSH Keys_. If your key not there, it can be added as depicted below.

![Add new SSH Key](_static/gitlab-add-new-ssh-key.png)

Next, I started to create a local documentation repository.

## Local repository and git init

I created a folder to host my local documentation framework. Inside it, I initialized git (which again came with my macOS version) and created `.gitignore` file used for git tracking process exemptions. Example point-in-time contents of `.gitignore` file are presented in the code snippet below. Adjust it for your own specific usage.

```bash
[~] mkdir documentation-as-code
[~] cd documentation-as-code
[documentation-as-code] git init
[documentation-as-code] touch .gitignore
```

```bash
[documentation-as-code] cat .gitignore
# directories
venv/
build/

# files
*.code-workspace
.DS_Store
```

Then, I configured git's basic settings applicable to my local documentation repository only. One can use global or custom settings as well.

```bash
[documentation-as-code] git init --initial-branch=main
[documentation-as-code] git config --local user.name "Tomasz Nonas"
[documentation-as-code] git config --local user.email "user@example.com"
[documentation-as-code] git remote add origin git@gitlab.com:tnonas/documentation-as-code.git
```

Please note, that I chose to use ssh-based method for git sync processes and this requires a local private key to be used for communication with GitLab. It is located in my local `~/.ssh` configuration directory and named `id_rsa`. Local username used for ssh session with GitLab does not have to match as it uses a generic `git` username with any local private key. As mentioned in [GitLab project](#gitlab-project) section, for this communication to work the corresponding public key (`id_rsa.pub` in my example) must be exported to GitLab beforehand. `tnonas/documentation-as-code.git` part comes directly from the GitLab project's URL.

## Python runtime environment

I used the Python version which came embedded with my macOS (Python 3.9.6).  Inside the documentation directory, I created a new Python virtual environment named _venv_. Then I activated it.

```bash
[~] cd documentation-as-code
[documentation-as-code] python3 -m venv venv
[documentation-as-code] source venv/bin/activate
```

Once inside _venv_, I updated pip and installed all needed Python modules listed below.

```bash
[documentation-as-code] pip3 install sphinx sphinx_rtd_theme sphinxcontrib-mermaid myst-parser
```

`sphinx_rtd_theme` is one of the most popular Sphinx documentation themes, `sphinxcontrib-mermaid` installs Mermaid diagrams as code support for Sphinx and `myst-parser` enables to use Markdown files as raw documentation sources, instead of the default reStructuredText format.

Following is the list of all modules and their dependencies required for the project.

```bash
(venv) [documentation-as-code] pip3 list
Package                       Version
----------------------------- --------
alabaster                     0.7.16
Babel                         2.14.0
certifi                       2024.2.2
charset-normalizer            3.3.2
docutils                      0.20.1
idna                          3.6
imagesize                     1.4.1
importlib_metadata            7.1.0
Jinja2                        3.1.3
markdown-it-py                3.0.0
MarkupSafe                    2.1.5
mdit-py-plugins               0.4.0
mdurl                         0.1.2
myst-parser                   2.0.0
packaging                     24.0
pip                           24.0
Pygments                      2.17.2
PyYAML                        6.0.1
requests                      2.31.0
setuptools                    58.0.4
snowballstemmer               2.2.0
Sphinx                        7.2.6
sphinx-rtd-theme              2.0.0
sphinxcontrib-applehelp       1.0.8
sphinxcontrib-devhelp         1.0.6
sphinxcontrib-htmlhelp        2.0.5
sphinxcontrib-jquery          4.1
sphinxcontrib-jsmath          1.0.1
sphinxcontrib-mermaid         0.9.2
sphinxcontrib-qthelp          1.0.7
sphinxcontrib-serializinghtml 1.1.10
urllib3                       2.2.1
zipp                          3.18.1
```

## Sphinx local deployment

